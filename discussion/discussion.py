# Python has severals structures to store collections or multiple items in a single variables

# Lists - are similar to arrays in JavaScript in a sense that they can contain a collectrion of data.
# To create a list, the square bracket([]) is used.
names = ["Mark", "Johan", "Charles", "Raf"]
programs = ["developer career", "pi-shape", "short courses"]
durations = [260, 180, 20]
truth_values = [True, False, True, True, False]

# A list can contain elements of different data types
sample_list = ["Apple", 3, False, "Potato", 4, True]
print(sample_list)

# Getting the list size - the number of elements in a list can be counted using the len() method
print(len(programs))

# Access the first item in the list
print(programs[0])

# Access the last item in the list
print(programs[-1])

# Access the second item in the list
print(durations[1])

# Access the whole list
print(durations)

# Access a range of values - list[start index:last index] 
# Note: the end index is not included
print(programs[0:2])

# Updating lists
print(f"Current value: {programs[2]}")
programs[2] = "Short Courses"
print(f"New value: {programs[2]}")

# mini-exercise
students = ["Mark", "Johan", "Charles", "Raf", "Ervin"]
student_grade = [1.2, 1.1, 1.3, 1.4, 1.2]
for x in range(len(students)):
	print(f"The grade of {students[x]} is {student_grade[x]}")


# List manipulation - has methods that can be used to manipulate the elements within

# Adding list items - append() method to insert items to a list
programs.append("global")
print(programs)

# Deleting list items - the "del" keyword is used
durations.append(360)
print(durations)
del durations[-1]
print(durations)

# Checking elements in the list - the "in" keyword is used
print(20 in durations)
print(500 in durations)

# Sorting elements in the list - the sort() method sorts the list alphanumerically, ascending by default
print(names)
names.sort()
print(names)

# Emptying the list - the clear() method is used to empty the contents of the list
test_list = [1,2,3,4,5]
print(test_list)
test_list.clear()
print(test_list)


# Dictionaries are used to store data values in key:value pairs
# To create a dictionary, the curly braces({}) is used and the key-value pairs are denoted with (key : value)
person1 = {
	"name": "Weigel",
	"age": 22,
	"occupation": "student",
	"isEnrolled": True,
	"subjects": ["Psychology", "Sociology", "Anthropology"]
}
print(person1)
print(len(person1))

# accessing values in dictionary
print(person1["name"])

# the keys() method will return a list of all the keys in the dictionary
print(person1.keys())

# the values() method will return a list of all the values in the dictionary
print(person1.values())

# the items() method will return each item in a dictionary in a key-value pair
print(person1.items())

# Adding key-value pairs can be done by putting a new index key-value or the update() method
person1["nationality"] = "Filipino"
person1.update({"fave_food": "Chicken"})
print(person1)

# Deleting entries can be done by pop() method or del keyword
person1.pop("fave_food")
del person1["nationality"]
print(person1)

# clear() method empties the dictionary
person2 = {
	"name": "John Faith",
	"occupation": "Priest"
}
print(person2)
person2.clear()
print(person2)

# Looping through Dictionaries
for key in person1:
	print(f"The value of {key} is {person1[key]}")

# Nested Dictionaries
person3 = {
	"name": "Apura",
	"age": 22,
	"occupation": "student",
	"isEnrolled": True,
	"subjects": ["Psychology", "Sociology", "Anthropology"]
}

classroom = {
	"student1": person1,
	"student2": person3
}
print(classroom)

# mini-exercise
car = {
	"brand": "Toyota",
	"model": "Fortuner",
	"year_of_make": 2011,
	"color": "White"
}
print(f"I own a {car['brand']} {car['model']} and it was made in {car['year_of_make']}.")

# Funtions - are blocks of code that runs when called
# the "def" keyword is used to create a function. syntax: def <functionName>()
def my_greeting():
	# code to be run when my_greeting() is called
	print("Hello User")

# Calling/Invoking a function
my_greeting()

# Parameters can be added to a function to have more control on what inputs for the function will be
def greet_user(username):
	print(f"Hello, {username}!")

greet_user("Bob")
greet_user("Amy")

# From a function's perspective: parameter is a variable inside the function definition and argument is the value that is sent to the function when it is called

# return statement - the "return" keyword allow functions to return values
def addition(num1, num2):
	return num1 + num2

sum = addition(1,2)
print(f"The sum is {sum}.")

# Lambda functions - a small anonymous function that can be used for callbacks
greeting = lambda person : f"Hello {person}"
print(greeting("John Wick"))
print(greeting("Mr. Bean"))

multiply = lambda a, b: a*b
print(multiply(5,6))
print(multiply(10,10))

# Classes - would serve as blueprints to describe the concepts of objects
# the "class" keyword is used along with class name to create a class

class Car():
	# properties/constructor
	def __init__(self, brand, model, year_of_make):
		super(Car, self).__init__()
		self.brand = brand
		self.model = model
		self.year_of_make = year_of_make
		# other properties can be added and assigned hard-coded values
		self.fuel = "Diesel"
		self.fuel_level = 0
	# methods
	def fillFuel(self):
		print(f"Current fuel level: {self.fuel_level}")
		print("filling up the fuel tank...")
		self.fuel_level = 100
		print(f"New fuel level: {self.fuel_level}")
		
	def drive(self, distance):
		print(f"The car is driven {distance} kilometers.")
		print(f"The fuel level left: {self.fuel_level - distance}")

# Creating an object is done by calling the class and provide the arguments
new_car = Car("Toyota", "Fortuner", "2011")

# Diplaying attributes - uses dot notation
print(f"My car is a {new_car.brand} {new_car.model}")

# Calling the method of the instance
new_car.fillFuel()
new_car.drive(50)